install.packages("ahp")
library(ahp)
#ahpFile <- system.file("aparaty.ahp", package="ahp")
#print(ahpFile)
myAhp <- Load("~/R/APU/Lab2/aparaty.ahp")

library(data.tree)
print(myAhp, filterFun = isNotLeaf)
print(myAhp, "weight")

Calculate(myAhp)
print(myAhp, "weight")

Visualize(myAhp)
AnalyzeTable(myAhp)

print(myAhp, priority = function(x) x$parent$priority["Total", x$name])

