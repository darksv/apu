#install.packages("mlr")
#install.packages("rFerns")
#install.packages("randomForestSRC")
#install.packages("ggplot2")

library("mlr")
library("rFerns")
library("randomForestSRC")
# listLearners()
# levels(factor(listLearners()$type))

data <- read.csv("~/R/APU/Lab1/aparaty.csv")
data$ocena <- factor(data$ocena)

data <- data[,4:9]
data

task = makeClassifTask(data=data, target = "ocena")

#l <- makeLearner("classif.C50")
rdesc <- makeResampleDesc("CV", iters = 10)
lrns_names <- c("lda", "rpart", "C50", "rFerns", "randomForestSRC")
lrns <- makeLearners(lrns_names, type = "classif")
#r <- resample(learner = l, task = task, resampling = rdesc, show.info = FALSE)

comp <- benchmark(learners = lrns, tasks=task, resamplings = rdesc)
comp

plt = plotBMRBoxplots(comp, measure = mmce, order.lrn = getBMRLearnerIds(comp))
plt

learner <- makeLearner("classif.lda")
training = train(learner, task)
predict(training, task)
