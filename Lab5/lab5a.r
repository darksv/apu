#install.packages("C50")

install.packages("mlr")
install.packages("rtree")
install.packages("rtree.plot")

library("C50")
library("MASS")
library("mlr")
library("rtree")
library("rtree.plot")

data(mtcars)
mtcars
mtcars$carb = factor(mtcars$carb)

tree <- rpart(carb~., mtcars)
rpart.plot(tree)

task <- makeClassifTask(id = 'dataset', data = mtcars, target = "carb");


learner <- makeLearner("classif.C50")
training <- train(learner, task)
actual <- predict(training, task)
actual
valid <- actual$data$truth == actual$data$response
accuracy <- sum(valid) / length(valid)
accuracy