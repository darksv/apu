install.packages("neuralnet")
library("neuralnet")

aparaty <- read.csv("~/R/APU/Lab1/aparaty.csv")
aparaty <- as.data.frame(aparaty)
aparaty
#data <- rbind(
#  rozdzielczosc = aparaty$rozdzielczosc,
#  zakres_czulosci = aparaty$zakres_czulosci,
#  liczba_opinii = aparaty$liczba_opinii,
#  status_opinii = aparaty$status_opinii,
#  cena = aparaty$cena
#)
#cena <- aparaty$cena

mpix <- as.character(aparaty$rozdzielczosc)
mpix_values <- substr(mpix, 1, nchar(mpix) - 5)
#aparaty$rozdzielczosc <- as.numeric(gsub(",", ".", mpix_values))

# aparaty$rozdzielczosc = as.integer(aparaty$rozdzielczosc)
#aparaty$zakres_czulosci = as.integer(aparaty$zakres_czulosci)
#aparaty$status_opinii = as.integer(aparaty$status_opinii)

inputs <- data.frame(
  rozdzielczosc = as.numeric(gsub(",", ".", mpix_values)),
  zakres_czulosci = as.numeric(aparaty$zakres_czulosci),
  liczba_opinii = as.numeric(aparaty$liczba_opinii),
  ocena = aparaty$ocena
)

input_mins <- apply(inputs[,1:4], 2, min)
input_maxs <- apply(inputs[,1:4], 2, max)
scaled_inputs <- as.data.frame(
  scale(inputs[,1:4], center = input_mins, scale = input_maxs)
)

outputs <- aparaty$cena

output_mins <- min(outputs)
output_maxs <- max(outputs)
scaled_outputs <- data.frame(
  cena=scale(outputs, center = output_mins, scale = output_maxs)
)

data <- cbind(scaled_inputs, cena=scaled_outputs)
data

net <- neuralnet(
  cena~rozdzielczosc+zakres_czulosci+liczba_opinii+ocena,
  data, 
  hidden = c(7, 5, 3), 
  threshold = 0.0001,
  stepmax = 1e6
)

actual <- compute(net, data)$net.result

rescaled_actual <- output_maxs * actual + output_mins
rescaled_actual
outputs
save(net, file = "~/R/APU/Lab3/net_b.rda")
plot(net)

