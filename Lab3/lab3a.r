install.packages("neuralnet")
library("neuralnet")

x <- seq(1, 16, 0.1)
y <- exp(sqrt(x))

data <- cbind(x=x, y=y)
colnames(data) <- c("x", "y")

net <- neuralnet(y~x, data, hidden = c(3,2), threshold = 0.01, stepmax = 1e6)
save(net, file = "~/R/APU/Lab3/net_a.rda")
plot(net)

xtest = seq(1,16,0.01)
y_actual = compute(net, as.data.frame(xtest))

plot(xtest, y_actual$net.result)

plot(x, y)

