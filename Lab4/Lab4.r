install.packages("C50")

library("C50")

aparaty <- read.csv("~/R/APU/Lab1/aparaty.csv")
aparaty
aparaty$ocena = factor(aparaty$ocena)
#head(aparaty)

aparaty$rozdzielczosc = gsub(",", ".", aparaty$rozdzielczosc)

tree <- C5.0(x=aparaty[,3:6], y=aparaty$ocena)
tree
summary(tree)
plot(tree)

